import { connDB } from './connectionDatabase.js';

export const cariSemester = async (idUser) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT DISTINCT semesterTahunAkademik FROM `sidang` WHERE idMhs = ? || idPembimbing = ? || idPenguji1 = ? || idPenguji2 = ?';
        conn.query(sql, [idUser, idUser, idUser, idUser], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const cariNilaiMhs = async (sem, sidangKe, npm) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
            if(sidangKe == 1){
                sql = `
                SELECT n1.idBobot, b1.nama AS namaKomponen, AVG(n1.nilai * b1.bobot / 100) AS nilaiRataRata, s.sidangKe 
                FROM Nilai1 n1 
                INNER JOIN Bobot1 b1 ON n1.idBobot = b1.idBobot 
                INNER JOIN Sidang s ON n1.idSidang = s.idSidang 
                WHERE s.sidangKe = ? AND s.semesterTahunAkademik = ? AND s.idMhs = (SELECT idUser FROM Pengguna WHERE nomorInduk = ?) 
                GROUP BY b1.nama;
                `;
            } 
            else {
                sql = `
                SELECT p.namaPeran, AVG(n.nilai * b.bobot / 100) AS nilaiRataRata, s.sidangKe
                FROM Nilai2 n
                INNER JOIN Bobot2 b ON n.idBobot = b.idBobot
                INNER JOIN Sidang s ON n.idSidang = s.idSidang
                INNER JOIN Pengguna m ON s.idMhs = m.idUser
                INNER JOIN Peran p ON b.idPeran = p.idPeran
                WHERE s.sidangKe = ?
                  AND s.semesterTahunAkademik = ?
                  AND m.nomorInduk = ?
                GROUP BY p.namaPeran;
                `;
            }
        conn.query(sql, [sidangKe, sem, npm], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getSidangKe_N = async (semester) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT DISTINCT sidangKe FROM `sidang` WHERE `semesterTahunAkademik` = ?';
        conn.query(sql, [semester], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getNpmBySemesterAndSidang = async (idUser, semester, sidangKe, id) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
        if(id == 1){
            sql = `
            SELECT idSidang, sidangKe, semesterTahunAkademik, DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal', TIME_FORMAT(waktu, '%H:%i') AS 'waktu', ruang, judul, isiCatatan, TablePeng1.status, TablePeng1.nomorInduk, namaMhs, idPembimbing, namaPembimbing, NIKPembimbing, namaPenguji1, NIKPenguji1, pengguna.nama AS namaPenguji2, pengguna.nomorInduk AS NIKPenguji2
            FROM 
            (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TablePemb.status, TablePemb.nomorInduk, namaMhs, idPembimbing, namaPembimbing, NIKPembimbing, idPenguji2, pengguna.nama AS namaPenguji1, pengguna.nomorInduk AS NIKPenguji1
                FROM 
                (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TableMhs.status, TableMhs.nomorInduk, namaMhs, idPembimbing, idPenguji1, idPenguji2, pengguna.nama AS namaPembimbing, pengguna.nomorInduk AS NIKPembimbing
                    FROM 
                    (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, pengguna.status, pengguna.nomorInduk, nama as "namaMhs", idPembimbing, idPenguji1, idPenguji2
                        FROM sidang 
                            JOIN pengguna ON sidang.idMhs = pengguna.idUser) AS TableMhs
                    JOIN pengguna ON TableMhs.idPembimbing = pengguna.idUser) AS TablePemb
                JOIN pengguna ON TablePemb.idPenguji1 = pengguna.idUser) AS TablePeng1
            JOIN pengguna ON TablePeng1.idPenguji2 = pengguna.idUser
            WHERE semesterTahunAkademik = ? && sidangKe = ?
            `;
            conn.query(sql, [semester, sidangKe], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        } 
        else {
            sql = `
            SELECT idSidang, sidangKe, semesterTahunAkademik, DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal', TIME_FORMAT(waktu, '%H:%i') AS 'waktu', ruang, judul, isiCatatan, TablePeng1.status, TablePeng1.nomorInduk, namaMhs, idPembimbing, namaPembimbing, NIKPembimbing, namaPenguji1, NIKPenguji1, pengguna.nama AS namaPenguji2, pengguna.nomorInduk AS NIKPenguji2
            FROM 
            (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TablePemb.status, TablePemb.nomorInduk, namaMhs, idPembimbing, namaPembimbing, NIKPembimbing, idPenguji2, pengguna.nama AS namaPenguji1, pengguna.nomorInduk AS NIKPenguji1
                FROM 
                (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TableMhs.status, TableMhs.nomorInduk, namaMhs, idPembimbing, idPenguji1, idPenguji2, pengguna.nama AS namaPembimbing, pengguna.nomorInduk AS NIKPembimbing
                    FROM 
                    (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, pengguna.status, pengguna.nomorInduk, nama as "namaMhs", idPembimbing, idPenguji1, idPenguji2
                        FROM sidang 
                            JOIN pengguna ON sidang.idMhs = pengguna.idUser) AS TableMhs
                    JOIN pengguna ON TableMhs.idPembimbing = pengguna.idUser) AS TablePemb
                JOIN pengguna ON TablePemb.idPenguji1 = pengguna.idUser) AS TablePeng1
            JOIN pengguna ON TablePeng1.idPenguji2 = pengguna.idUser
            WHERE (TablePeng1.nomorInduk = ? || NIKPembimbing = ? || NIKPenguji1 = ? || Pengguna.nomorInduk = ?) && semesterTahunAkademik = ? && sidangKe = ?;
        `;
            conn.query(sql, [idUser, idUser, idUser, idUser, semester, sidangKe], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
            });
        }
        conn.release();
    });
};

export const ubahNilai = async (idNilai, nilai, sidangKe) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
            let sql3;
            if(sidangKe == 1){
                sql3 = `
                UPDATE nilai1
                SET nilai = ?
                WHERE idNilai = ?; 
                `;
            } else {
                sql3 = `
                UPDATE nilai2
                SET nilai = ?
                WHERE idNilai = ?; 
                `;
            }
            conn.query(sql3, [nilai, idNilai], (err1, result1) => {
                if (err1) {
                    reject(err1);
                } else {
                    result1 = true
                    resolve(result1);
            }
        });
        conn.release();
    });
}

export const getNilai1 = async (sem, sidangKe, npm) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
    SELECT DISTINCT
        Pengguna.nama AS student_name,
        Sidang.semesterTahunAkademik AS semester,
        Bobot1.nama AS komponen_nilai,
        Bobot1.bobot AS komponen_bobot,
        Nilai1.nilai AS nilai,
        Nilai1.idNilai AS idNilai, 
        sidang.idPembimbing AS idPembimbing,
        sidang.idPenguji1 AS idPenguji1,
        sidang.idPenguji2 AS idPenguji2,
        sidang.sidangKe,
        Bobot1.idBobot AS id_komponen_nilai,
        sidang.idSidang,
        Bobot1.idPeran
    FROM
        Pengguna
    JOIN Sidang ON Pengguna.idUser = Sidang.idMhs
    JOIN Bobot1 ON Sidang.semesterTahunAkademik = Bobot1.semesterTahunAkademik
    JOIN Nilai1 ON Sidang.idSidang = Nilai1.idSidang AND Bobot1.idBobot = Nilai1.idBobot
    WHERE
        Bobot1.semesterTahunAkademik = ? AND
        Sidang.sidangKe = ? AND
        Pengguna.nomorInduk = ?;
`;

        conn.query(sql, [sem, sidangKe, npm], (err, result) => {
            if (err) {
                reject(err);
            } else {
                //hasilNilai1 = result;
                resolve(result);
            }
            conn.release();
        });
    });
};

export const getNilai2BySemesterAndNPM = async (sem, sidangKe, npm) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
            SELECT DISTINCT
                Pengguna.nama AS student_name,
                Sidang.semesterTahunAkademik AS semester,
                Bobot2.nama AS komponen_nilai,
                Bobot2.bobot AS komponen_bobot,
                Nilai2.nilai AS nilai,
                Nilai2.idNilai AS idNilai, 
                sidang.idPembimbing AS idPembimbing,
                sidang.idPenguji1 AS idPenguji1,
                sidang.idPenguji2 AS idPenguji2,
                sidang.sidangKe,
                Bobot2.idBobot AS id_komponen_nilai,
                sidang.idSidang,
                Bobot2.idPeran
            FROM
                Pengguna
            JOIN Sidang ON Pengguna.idUser = Sidang.idMhs
            JOIN Bobot2 ON Sidang.semesterTahunAkademik = Bobot2.semesterTahunAkademik
            JOIN Nilai2 ON Sidang.idSidang = Nilai2.idSidang AND Bobot2.idBobot = Nilai2.idBobot
            WHERE
                Bobot2.semesterTahunAkademik = ? AND
                Sidang.sidangKe = ? AND
                Pengguna.nomorInduk = ?;
        `;

        conn.query(sql, [sem, sidangKe, npm], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
            conn.release();
        });
    });
};