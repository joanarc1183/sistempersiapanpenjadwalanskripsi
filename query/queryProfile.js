import { connDB } from './connectionDatabase.js';

export const getUserDosen = async (idUser) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT Pengguna.nama, Pengguna.nomorInduk,  Pengguna.email FROM Pengguna WHERE  idUser = ?';
        conn.query(sql, [idUser], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
            conn.release();
        });
    });
};

export const getUserMhs = async (idUser) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT Pengguna.nama, Pengguna.nomorInduk, Pengguna.email FROM Pengguna WHERE  idUser = ?';
        conn.query(sql, [idUser], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
            conn.release();
        });
    });
};
