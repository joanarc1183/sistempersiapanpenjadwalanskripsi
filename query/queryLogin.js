import { connDB } from './connectionDatabase.js';

export const cekPengguna = async (username, password) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        conn.query("SELECT * FROM `pengguna` WHERE username LIKE BINARY ? AND password LIKE BINARY ?", [username, password], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
};