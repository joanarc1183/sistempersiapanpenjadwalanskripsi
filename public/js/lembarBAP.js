const skripsiSelect = document.querySelector("#skripsi-dropdown");
const semSelect = document.querySelector("#semester-dropdown");

//menampilkan dropdown semester dan tahun akademik
async function getSems() {
 
        fetch("/getSems").then(onSuccess).then(showSem);

        function onSuccess(response) {
            return response.json();
        }

        function showSem(result) {
            for (let i = 0; i < result.length; i++) {
                const semOption = document.createElement("option");
                semOption.value = result[i][0];
                semOption.textContent = result[i][0];
                semSelect.appendChild(semOption);
            }
        }
    
}

window.onload = function() {
    getSems();
}

semSelect.addEventListener('click', async () => {
    let thnAkad = semSelect.value;

    let sk;
    if (skripsiSelect.value == "Skripsi 1"){
        sk = 1
    } else if (skripsiSelect.value == "Skripsi 2"){
        sk = 2
    } else {
        sk = skripsiSelect.value
    }
    
    let data = {
        thn: thnAkad,
        sk: sk
    };
    const params = new URLSearchParams(data);
    const q = params.toString();
    const url = "/getSkrip?" + q;
    fetch(url).then(onSuccess).then(getSkrip);
    function onSuccess(response) {
        return response.json();
    }

    //menampilkan dropdown skripsi pada semester dan tahun akademik yang dipilih
    function getSkrip(data) {
        const resultSkripsiOpt = data.skripsiO;
        const resultJadwal = data.jadwal;

        //kosongkan option
        let i = skripsiSelect.childElementCount;
        while(i > 1){
            skripsiSelect.removeChild(skripsiSelect.children[i-1])
            i--;
        }
        //tambahkan option
        for (let i = 0; i < resultSkripsiOpt.length; i++) {
            const skripsiOption = document.createElement("option");
            if(resultSkripsiOpt[i][0] == 1){
                skripsiOption.value = "Skripsi 1";
                skripsiOption.textContent = "Skripsi 1";
            }else{
                skripsiOption.value = "Skripsi 2";
                skripsiOption.textContent = "Skripsi 2";
            }
            skripsiSelect.appendChild(skripsiOption);
        }
        
        // untuk menampilkan tabel
        const tabelDosen = document.querySelector("#tabelDosen tbody")

        const tabelMhs = document.querySelector("#tabelMhs tbody")
        
        if(data.statusDosen){
            // delete isi tabel bila sebelumnya ada tabel yang ditampilkan
            while (tabelDosen.rows.length > 0){
                tabelDosen.deleteRow(0);
            }

            for (let i=0; i<resultJadwal.length; i++){
                const tr = document.createElement("tr");
    
                const td1 = document.createElement("td");
                td1.textContent = (i+1);
                tr.appendChild(td1)
    
                const td2 = document.createElement("td");
                td2.textContent = resultJadwal[i][3];
                tr.appendChild(td2);
    
                const td3 = document.createElement("td");
                td3.textContent = resultJadwal[i][4];
                tr.appendChild(td3);
                
                const td4 = document.createElement("td");
                td4.textContent = resultJadwal[i][1];
                tr.appendChild(td4);
                
                const td5 = document.createElement("td");
                td5.textContent = resultJadwal[i][2];
                tr.appendChild(td5);
    
                const td6 = document.createElement("td");
                td6.textContent = resultJadwal[i][5];
                tr.appendChild(td6);
    
                const td7 = document.createElement("td");
                const download = document.createElement("button");
                download.textContent = "download";
                download.value = resultJadwal[i][0];
                td7.appendChild(download);
                td7.classList.add('btnDownload')
                tr.appendChild(td7);
    
                const td8 = document.createElement("td");
                const upload = document.createElement("button");
                upload.textContent = "upload";
                upload.value = resultJadwal[i][0];
                upload.setAttribute('disabled', true);
                td8.appendChild(upload);
                td8.classList.add('btnUpload')
                tr.appendChild(td8);
    
                tabelDosen.appendChild(tr);
            }
    
        }else{
            // delete isi tabel bila sebelumnya ada tabel yang ditampilkan
            while (tabelMhs.rows.length > 0){
                tabelMhs.deleteRow(0);
            }

            for (let i=0; i<resultJadwal.length; i++){
                const tr = document.createElement("tr");
    
                const td1 = document.createElement("td");
                td1.textContent = (i+1);
                tr.appendChild(td1)
                
                const td4 = document.createElement("td");
                td4.textContent = resultJadwal[i][1];
                tr.appendChild(td4);
                
                const td5 = document.createElement("td");
                td5.textContent = resultJadwal[i][2];
                tr.appendChild(td5);
    
                const td6 = document.createElement("td");
                td6.textContent = resultJadwal[i][3];
                tr.appendChild(td6);
    
                const td7 = document.createElement("td");
                const download = document.createElement("button");
                download.textContent = "download";
                download.value = resultJadwal[i][0];
                td7.appendChild(download);
                td7.classList.add('btnDownload')
                tr.appendChild(td7);
    
                const td8 = document.createElement("td");
                const upload = document.createElement("button");
                upload.textContent = "upload";
                upload.value = resultJadwal[i][0];
                upload.setAttribute('disabled', true);
                td8.appendChild(upload);
                td8.classList.add('btnUpload')
                tr.appendChild(td8);
    
                tabelMhs.appendChild(tr);
            }
    
        }
        downloadUpload()
    }
})

skripsiSelect.addEventListener('click', async () => {
    let thnAkad = semSelect.value;
    let sk;
    if (skripsiSelect.value == "Skripsi 1"){
        sk = 1
    } else if (skripsiSelect.value == "Skripsi 2"){
        sk = 2
    } else {
        sk = skripsiSelect.value
    }

    let data = {
        thn: thnAkad,
        sk: sk
    };
    const params = new URLSearchParams(data);
    const q = params.toString();
    const url = "/getSkrip?" + q;
    fetch(url).then(onSuccess).then(getSkrip);
    function onSuccess(response) {
        return response.json();
    }

    //menampilkan dropdown skripsi pada semester dan tahun akademik yang dipilih
    function getSkrip(data) {
        const resultJadwal = data.jadwal;

        
        // untuk menampilkan tabel
        const tabelDosen = document.querySelector("#tabelDosen tbody")
        const tabelMhs = document.querySelector("#tabelMhs tbody")

        if(data.statusDosen){
                    // delete isi tabel bila sebelumnya ada tabel yang ditampilkan
            while (tabelDosen.rows.length > 0){
                tabelDosen.deleteRow(0);
            }

            for (let i=0; i<resultJadwal.length; i++){
                const tr = document.createElement("tr");
    
                const td1 = document.createElement("td");
                td1.textContent = (i+1);
                tr.appendChild(td1)
    
                const td2 = document.createElement("td");
                td2.textContent = resultJadwal[i][3];
                tr.appendChild(td2);
    
                const td3 = document.createElement("td");
                td3.textContent = resultJadwal[i][4];
                tr.appendChild(td3);
                
                const td4 = document.createElement("td");
                td4.textContent = resultJadwal[i][1];
                tr.appendChild(td4);
                
                const td5 = document.createElement("td");
                td5.textContent = resultJadwal[i][2];
                tr.appendChild(td5);
    
                const td6 = document.createElement("td");
                td6.textContent = resultJadwal[i][5];
                tr.appendChild(td6);
    
                const td7 = document.createElement("td");
                const download = document.createElement("button");
                download.textContent = "download";
                download.value = resultJadwal[i][0];
                td7.appendChild(download);
                td7.classList.add('btnDownload')
                tr.appendChild(td7);
    
                const td8 = document.createElement("td");
                const upload = document.createElement("button");
                upload.textContent = "upload";
                upload.value = resultJadwal[i][0];
                upload.setAttribute('disabled', true);
                td8.appendChild(upload);
                td8.classList.add('btnUpload')
                tr.appendChild(td8);
    
                tabelDosen.appendChild(tr);
            }
            
        }else{
                    // delete isi tabel bila sebelumnya ada tabel yang ditampilkan
            while (tabelMhs.rows.length > 0){
                tabelMhs.deleteRow(0);
            }

            for (let i=0; i<resultJadwal.length; i++){
                const tr = document.createElement("tr");
    
                const td1 = document.createElement("td");
                td1.textContent = (i+1);
                tr.appendChild(td1)
                
                const td4 = document.createElement("td");
                td4.textContent = resultJadwal[i][1];
                tr.appendChild(td4);
                
                const td5 = document.createElement("td");
                td5.textContent = resultJadwal[i][2];
                tr.appendChild(td5);
    
                const td6 = document.createElement("td");
                td6.textContent = resultJadwal[i][3];
                tr.appendChild(td6);
    
                const td7 = document.createElement("td");
                const download = document.createElement("button");
                download.textContent = "download";
                download.value = resultJadwal[i][0];
                td7.appendChild(download);
                td7.classList.add('btnDownload')
                tr.appendChild(td7);
    
                const td8 = document.createElement("td");
                const upload = document.createElement("button");
                upload.textContent = "upload";
                upload.value = resultJadwal[i][0];
                upload.setAttribute('disabled', true);
                td8.appendChild(upload);
                td8.classList.add('btnUpload')
                tr.appendChild(td8);
    
                tabelMhs.appendChild(tr);
            }
        }
        downloadUpload()
    }
})

function downloadUpload() {
    const btnDownload = document.querySelectorAll("td.btnDownload button")
    const btnUpload = document.querySelectorAll("td.btnUpload button")
    btnDownload.forEach(element => {
        element.addEventListener('click', ()=>{
            btnUpload.forEach(element1 => {
                if (element.value == element1.value){
                    element1.removeAttribute('disabled');
                }
            });
        })
    });
}