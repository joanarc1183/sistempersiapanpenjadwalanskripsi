import { connDB } from './connectionDatabase.js';

export const jadwalKoordinator = async (data) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT * FROM sidang WHERE nik = ? && idPeran = "KDS"';
        conn.query(sql, data, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}
