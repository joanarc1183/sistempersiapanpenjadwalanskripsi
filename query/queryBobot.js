import { connDB } from './connectionDatabase.js';

export const getSemester = async (sem) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT semesterTahunAkademik FROM ?';
        conn.query(sql, sem, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
        conn.release();
    })
}

export const getBobot1 = async (sem) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT DISTINCT idKomponen, nama, bobot AS bobotKomp FROM Bobot1 WHERE semesterTahunAkademik = ?';
        conn.query(sql, [sem], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
        conn.release();
    })
}

export const getBobot2 = async (sem) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT DISTINCT Bobot2.idPeran AS id, Peran.namaPeran, bobot FROM Bobot2 JOIN Peran ON Bobot2.idPeran = Peran.idPeran WHERE Bobot2.semesterTahunAkademik = ?';
        conn.query(sql, [sem], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
        conn.release();
    })
}

export const updateBobot1 = async (namaKomp, bobotKomp, idKomp, curSem) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        UPDATE bobot1
        SET nama = ?, bobot = ?
        WHERE idKomponen = ? AND semesterTahunAkademik = ?; `;
        conn.query(sql, [namaKomp, bobotKomp, idKomp, curSem], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
        conn.release();
    })
}

export const updateBobot2 = async (namaKomp, bobotKomp, idPeran, curSem) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        UPDATE bobot2
        SET nama = ?, bobot = ?
        WHERE idPeran = ? AND semesterTahunAkademik = ?; `;
        conn.query(sql, [namaKomp, bobotKomp, idPeran, curSem], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
        conn.release();
    })
}
