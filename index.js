import express from 'express';
import bodyParser from 'body-parser';
import mysql from 'mysql';
import crypto from 'crypto';
import session from 'express-session';
import memoryStore from 'memorystore';

const PORT = 8080;
const app = express();

const SessionStore = memoryStore(session);
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(session({
    store: new SessionStore({
        checkPeriod: 10*60*60*1000   //ms
    }),
    name: 'nama',
    secret: 'rahasiasecret',
    resave: false,
    saveUninitialized: true
}));

app.listen(PORT, (err) => {
    if (err){
        console.error('Server error');
    } else {
        console.log(`Server is ready, listening on port ${PORT}`);
    }
});

export const conn = mysql.createConnection({
    user: 'root',
    password: '',
    database: 'sistem_persiapan_penjadwalan_skripsi',
    host: 'localhost'
});

// middleware
const auth = (req, res, next) => {
    if(!req.session.username){
        res.render('login', {
            cek: true
        });
    }else{
        next();
    }
}

//PROFILE PAGE ------------------------------------------------------------------------------------------------
import {getUserDosen, getUserMhs} from './query/queryProfile.js'
app.get('/profile', auth, async (req, res) => {
    let dosen = true;
    let profile;
    const idU = req.session.idUser;
    if(req.session.status==1){
        dosen = true;
    }else{
        dosen = false;
    }

    if(dosen){
        profile = await getUserDosen(req.session.idUser);
    }else{
        profile = await getUserMhs(req.session.idUser);
    }

    res.render('profile', {
        idUser : idU,
        Pengguna : idU,
        profile : profile,
        dosen : dosen,

        //buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

//LEMBAR BAP PAGE -------------------------------------------------------------------------------------------------
import {getBAP_Koord, getBAP_Dosen, getBAP_Mhs, getSems, getSkrip} from './query/queryLembarBAP.js';
let dosen;
app.get('/lembarBAP', auth, async (req, res) => {
    let skripsi;
    const idU = req.session.idUser;
    if(req.session.idUser==1){
        cekKoordinator = true;
    }else{
        cekKoordinator = false;
    }

    if(req.session.status==1){
        dosen = true;
    }else{
        dosen = false;
    }

    if(cekKoordinator == true){
        skripsi = await getBAP_Koord(req.session.idUser);
    }else if (dosen){
        skripsi = await getBAP_Dosen(req.session.idUser);
    }else{
        skripsi = await getBAP_Mhs();
    }

    res.render('lembarBAP', {
        Pengguna : idU,
        skripsiData : skripsi,
        dosen : dosen,
        skripsi : skripsi,
        //buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

app.get('/getSems', async(req, res) => {
    let sem
    await getSems().then((result)=>{
        sem = [];
        for(let i of result){
            sem.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });
    res.json(sem);
});

app.get('/getSkrip', async(req, res) => {
    let thnakd = req.query.thn;
    let sk = req.query.sk;

    if(sk == null){
        sk = "all"
    }
    if (thnakd == null){
        thnakd = "GANJIL2023-2024"
    }

    let skripsiOpt;
    await getSkrip(thnakd).then((result)=>{
        skripsiOpt = [];
        for(let i of result){
            skripsiOpt.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });

    let jadwal;
    if (cekKoordinator == true){
        await getBAP_Koord(thnakd, sk).then((result)=>{
            jadwal = [];
            for(let i of result){
                jadwal.push(Object.values(JSON.parse(JSON.stringify(i))));
            }
        })
    }else if (dosen){
        await getBAP_Dosen(req.session.idUser, thnakd, sk).then((result)=>{
            jadwal = [];
            for(let i of result){
                jadwal.push(Object.values(JSON.parse(JSON.stringify(i))));
            }
        })
    } else {
        await getBAP_Mhs(req.session.idUser, thnakd, sk).then((result)=>{
            jadwal = [];
            for(let i of result){
                jadwal.push(Object.values(JSON.parse(JSON.stringify(i))));
            }
        })
    }
    const data = {
        skripsiO: skripsiOpt,
        jadwal: jadwal,
        statusDosen: dosen,
        statusKoord : cekKoordinator,
    }
    res.json(data);
});

//LOG IN----------------------------------------------------------------------------------------------------------
import { cekPengguna } from "./query/queryLogin.js";
let cek= false;
app.get(['/','/login'], (req, res) => {
    res.render('login', {
        cek,
    });
    cek = false;
});

let username;
let password;
let cekKoordinator = false;

app.post('/login', async(req,res) =>{
    //mengambil username dan password yg diinput user
    username = req.body.username;
    password = crypto.createHash('sha256').update(req.body.password).digest('base64');
    if (username && password) {
        try {
            const data = await cekPengguna(username, password);
            const res_data = JSON.parse(JSON.stringify(data))[0];

            if (res_data !== undefined) {
                // Tambahkan session
                const session = req.session;
                session.username = res_data.username;
                session.password = res_data.password;
                session.idUser = res_data.idUser;
                session.nama = res_data.nama;
                session.nomorInduk = res_data.nomorInduk;
                session.status = res_data.status;

                if (session.idUser === 1) {
                    // Cek jika user adalah koordinator
                    cekKoordinator = true;
                }
                res.redirect('/jadwal');

            } else {
                cek = true;
                res.redirect('/');
            }
        } catch (error) {
            // Handle error
            res.redirect('/');
        }
    } else {
        res.redirect('/');
    }
});


//CATATAN SIDANG--------------------------------------------------------------------------------------------------
import { catatanPerUser, get_sidangKe, get_npm, catatan, uploadCatatan } from './query/queryCatatan.js';

let cariCatatanSidang = false;
let bisaEditCat = false;

app.get('/catatanSidang', auth, async (req, res) => {
    if(req.session.idUser==1){
        cekKoordinator = true;
    }else{
        cekKoordinator = false;
    }

    const catPerUser = await catatanPerUser(req.session.idUser);
    cariCatatanSidang = false;
    bisaEditCat = false;

    res.render('catatanSidang', {
        catSidang: catPerUser,
        cari: cariCatatanSidang,
        bisaEdit: bisaEditCat,
        dosenMhs: req.session.status,
        //ini buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

app.get("/getSidangKe", (req, res) => {
    const t = req.query.thnAkad;
    get_sidangKe(t).then((result)=>{
        let sidangKe = [];
        for(let i of result){
            sidangKe.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
        res.json(sidangKe);
    });
});

app.get("/getNPM", (req, res) => {
    const t = req.query.thnAkad;
    const l = req.query.sdgKe;
    get_npm( req.session.nomorInduk, t, l).then((result)=>{
        let npm = [];
        for(let i of result){
            npm.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
        res.json(npm);
    });
});

app.get("/uploadCatatan", async (req, res) => {
    const c = req.query.catatan;
    await uploadCatatan(c).then((result)=>{
        res.json(result);
    });
});

app.post("/catatanSidang", auth, async (req, res) => {
    const catPerUser = await catatanPerUser(req.session.idUser);
    cariCatatanSidang = true

    let cat;
    if (req.session.status == 1){
        const { thnAkad, sidangKe, npmMhs } = req.body;
        cat = await catatan(thnAkad, sidangKe, npmMhs);
        if (cat[0].idPembimbing == req.session.idUser && cat[0].isiCatatan == ""){
            // posisi jadi pembimbing, bisa edit catatan
            bisaEditCat = true
        }
    } else {
        const { thnAkad, sidangKe } = req.body;
        cat = await catatan(thnAkad, sidangKe, req.session.nomorInduk);
    }

    res.render('catatanSidang', {
        catSidang: catPerUser,
        cari: cariCatatanSidang,
        bisaEdit: bisaEditCat,
        dosenMhs: req.session.status,
        catatan: cat,
        //ini buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

//EDIT NILAI------------------------------------------------------------------------------------------------------
import { cariSemester, getSidangKe_N, getNpmBySemesterAndSidang, getNilai1, getNilai2BySemesterAndNPM, ubahNilai, cariNilaiMhs } from './query/queryNilai.js';

let bobotNilai;
let nilaiMhs;
app.get('/nilai', auth, async (req, res) => {
    let defVar = false;
    if(req.session.idUser==1){
        cekKoordinator = true;
    }else{
        cekKoordinator = false;
    }
    const cSemester = await cariSemester(req.session.idUser);

    let nilaiSem = req.query.thnAkad;
    let nilaiSkripsi = req.query.sdgKe;
    let bobotNilai = await getNilai1(nilaiSem);
    if(nilaiSkripsi== 1){
        bobotNilai = await getNilai1(nilaiSem);
    }else{
        bobotNilai = await getNilai2BySemesterAndNPM(nilaiSem);
    }
    
    res.render('nilai', {
        idDosenMhs: req.session.status,
        semesters: cSemester,
        definedVariable: defVar,
        komponenNilai: bobotNilai,
        //ini buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

app.get("/getSidangKeNilai", (req, res) => {
    const t = req.query.thnAkad;
    getSidangKe_N(t).then((result)=>{
        let sidangKe = [];
        for(let i of result){
            sidangKe.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
        res.json(sidangKe);
    });
});

app.get("/getNPMNilai", (req, res) => {
    const t = req.query.thnAkad;
    const l = req.query.sdgKe;
    getNpmBySemesterAndSidang(req.session.nomorInduk, t, l, req.session.idUser).then((result)=>{
        let npm = [];
        for(let i of result){
            npm.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
        res.json(npm);
    });
});

app.get("/uploadNilai", auth, async (req, res) => {
    let nilai = req.query.arrN
    const arr = nilai.split(",");
    for (let i=0; i<arr.length; i++){
        if (arr[i] != ""){
            await ubahNilai(bobotNilai[i].idNilai, arr[i], bobotNilai[i].sidangKe)
        }
    }
    res.redirect('/nilai');
});


app.post('/nilai', auth, async (req, res) => {
    let { semester, skripsi, npm } = req.body;
    let defVar = true;
    const cSemester = await cariSemester(req.session.idUser);

    bobotNilai = await getNilai1(semester, skripsi, npm);
    let peran = "";
    if(req.session.status == 1){
        if(skripsi== 1){
            bobotNilai = await getNilai1(semester, skripsi, npm);
        }else{
            bobotNilai = await getNilai2BySemesterAndNPM(semester, skripsi, npm);
        }
        
        if (bobotNilai[0].idPembimbing == req.session.idUser){
            peran = "PB";
        } else if (bobotNilai[0].idPenguji1 == req.session.idUser){
            peran = "PJ1";
        } else if (bobotNilai[0].idPenguji2 == req.session.idUser) {
            peran = "PJ2";
        } else {
            peran = "K";
        }
    } else if (req.session.status == 0){
        nilaiMhs = await cariNilaiMhs(semester, skripsi, req.session.nomorInduk);
    }
    
    res.render('nilai', {
        definedVariable: defVar,
        userRole: peran,
        semesters: cSemester,
        idDosenMhs: req.session.status,
        komponenNilai: bobotNilai,
        cekStatus: req.session.status,
        liatNilai: nilaiMhs,
        //ini buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});



//JADWAL ---------------------------------------------------------------------------------------------------------
import { getJadwalKoord, getJadwalDosen, getJadwalMhs, getDetail, getSem, getSkripsi, getRuangAdd, getMhsAdd, getDosenAdd, insertNilai1, insertNilai2, getCurId, insertSidang } from './query/queryJadwal.js';
app.get('/jadwal', auth, async(req, res) => {
    if(req.session.idUser==1){
        cekKoordinator = true;
    }else{
        cekKoordinator = false;
    }
    res.render('jadwal', {
        //buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

app.get('/getSem', async(req, res) => {
    let sem
    await getSem().then((result)=>{
        sem = [];
        for(let i of result){
            sem.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });
    res.json(sem);
});

app.get('/getSkripsi', async(req, res) => {
    let thnakd = req.query.thn;
    let sk = req.query.sk;

    if(sk == null){
        sk = "all"
    }
    if (thnakd == null){
        thnakd = "GANJIL2023-2024"
    }

    let skripsiOpt;
    await getSkripsi(thnakd).then((result)=>{
        skripsiOpt = [];
        for(let i of result){
            skripsiOpt.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });

    let jadwal;
    if (cekKoordinator == true){
        await getJadwalKoord(thnakd, sk).then((result)=>{
            jadwal = [];
            for(let i of result){
                jadwal.push(Object.values(JSON.parse(JSON.stringify(i))));
            }
        })
    } else if (req.session.status == 1){
        await getJadwalDosen(thnakd, sk, req.session.idUser).then((result)=>{
            jadwal = [];
            for(let i of result){
                jadwal.push(Object.values(JSON.parse(JSON.stringify(i))));
            }
        })
    } else {
        await getJadwalMhs(thnakd, sk, req.session.idUser).then((result)=>{
            jadwal = [];
            for(let i of result){
                jadwal.push(Object.values(JSON.parse(JSON.stringify(i))));
            }
        })
    }
    const data = {
        skripsiO: skripsiOpt,
        jadwal: jadwal
    }
    res.json(data);
});

//detail jadwal
app.get('/getDetail', async(req, res) => {
    let getView = req.query.cur;
    let detail;
    await getDetail(getView).then((result)=>{
        detail = [];
        for(let i of result){
            detail.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });
    res.json(detail);
});


//add jadwal
let semAdd, skripsiAdd;
app.get('/semSkripsiAdd', async(req) => {
    semAdd = req.query.semAdd
    skripsiAdd = req.query.skripsiAdd
});

app.get('/getRuangAdd', async(req, res) => {
    let ruang;
    await getRuangAdd().then((result)=>{
        ruang = [];
        for(let i of result){
            ruang.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });
    res.json(ruang);
});

app.get('/getMhsAdd', async(req, res) => {
    let mhs;
    await getMhsAdd(semAdd, skripsiAdd).then((result)=>{
        mhs = [];
        for(let i of result){
            mhs.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });
    res.json(mhs);
});

app.get('/getDosenAdd', async(req, res) => {
    let dos;
    await getDosenAdd().then((result)=>{
        dos = [];
        for(let i of result){
            dos.push(Object.values(JSON.parse(JSON.stringify(i))));
        }
    });
    res.json(dos);
});

app.post('/addJadwal', auth, async (req, res) => {
    const {skripsi, semester, tanggalSidang, waktuSidang, ruangSidang, npmMhs, judulSkripsi, pbu, pu1, pu2} = req.body;
    const cur = await getCurId();
    const curId = cur[0].idSidang + 1;
    await insertSidang(skripsi, semester, tanggalSidang, waktuSidang, ruangSidang, judulSkripsi, npmMhs, pbu, pu1, pu2);
    if(skripsiAdd == 1){
        if(semAdd == "GANJIL2023-2024"){
            for(let i = 1; i<=5; i++){
                await insertNilai1(curId, i)
            }
        } else{
            for(let i = 6; i<=10; i++){
                await insertNilai1(curId, i)
            }
        }
    } else{
        if(semAdd == "GANJIL2023-2024"){
            for(let i = 1; i<=17; i++){
                await insertNilai2(curId, i)
            }
        } else{
            for(let i = 18; i<=34; i++){
                await insertNilai2(curId, i)
            }
        }
    }
    res.redirect('/jadwal');
});


//BOBOT NILAI-----------------------------------------------------------------------------------------------------
import { getBobot1, getBobot2, updateBobot1, updateBobot2 } from './query/queryBobot.js';
app.get('/bobotNilai', auth, async (req, res) => {
    res.render('bobotNilai',{
        //buat header
        cekKoordinator: cekKoordinator,
        nama: req.session.nama
    });
});

app.get('/getBobot', async (req, res) => {
    let bobotSem, bobotSkripsi, bobotTable;
    bobotSem = req.query.sem;
    bobotSkripsi = req.query.skripsi;
    if(bobotSkripsi==1){
        bobotTable = await getBobot1(bobotSem);
    }else{
        bobotTable = await getBobot2(bobotSem);
    }
    let bobot = [];
    for(let i of bobotTable){
        bobot.push(Object.values(JSON.parse(JSON.stringify(i))));
    }
    res.json(bobot);
});

app.post('/updateBobot', auth, async(req) => {
    if(req.body.skripsi == 1){
        const {id1, id2, id3, id4, id5, nama1, nama2, nama3, nama4, nama5, bobot1, bobot2, bobot3, bobot4, bobot5, sem} = req.body
        await updateBobot1(nama1, bobot1, id1, sem)
        await updateBobot1(nama2, bobot2, id2, sem)
        await updateBobot1(nama3, bobot3, id3, sem)
        await updateBobot1(nama4, bobot4, id4, sem)
        await updateBobot1(nama5, bobot5, id5, sem)
    }else{
        const {id1, id2, id3, id4, nama1, nama2, nama3, nama4, bobot1, bobot2, bobot3, bobot4, sem} = req.body
        await updateBobot2(nama1, bobot1, id1, sem)
        await updateBobot2(nama2, bobot2, id2, sem)
        await updateBobot2(nama3, bobot3, id3, sem)
        await updateBobot2(nama4, bobot4, id4, sem)
    }
})
