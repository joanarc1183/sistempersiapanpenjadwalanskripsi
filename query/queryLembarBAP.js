import { connDB } from './connectionDatabase.js';

export const getBAP_Koord = async (thnakd, sk) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
        if(sk == 1 || sk ==2){
            sql = `
                SELECT
                    sidang.idSidang as 'idSidang',
                    sidang.sidangKe as 'sidangKe',
                    sidang.semesterTahunAkademik as 'thnAkad', 
                    mhs.nomorInduk ,
                    mhs.nama as 'nama',
                    sidang.judul as 'judul',
                    sidang.fileBap as 'file'
                FROM sidang
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE sidang.semesterTahunAkademik = ? AND sidang.sidangKe = ?`;
            

            conn.query(sql, [thnakd, sk], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
                
            });
        }else{
            sql = `
                SELECT
                    sidang.idSidang as 'idSidang',
                    sidang.sidangKe as 'sidangKe',
                    sidang.semesterTahunAkademik as 'thnAkad', 
                    mhs.nomorInduk ,
                    mhs.nama as 'nama',
                    sidang.judul as 'judul',
                    sidang.fileBap as 'file'
                FROM sidang
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE sidang.semesterTahunAkademik = ?
                `;
        
            conn.query(sql, [thnakd], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
                
            });

        }
            conn.release();
        });
    }


export const getBAP_Dosen = async (idDosen, thnakd, sk) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
        if(sk == 1 || sk ==2){
            sql = `
                SELECT
                    sidang.idSidang as 'idSidang',
                    sidang.sidangKe as 'sidangKe',
                    sidang.semesterTahunAkademik as 'thnAkad', 
                    mhs.nomorInduk ,
                    mhs.nama as 'nama',
                    sidang.judul as 'judul',
                    sidang.fileBap as 'file'
                FROM sidang
                JOIN pengguna pembimbing ON sidang.idPembimbing = pembimbing.idUser
                JOIN pengguna penguji1 ON sidang.idPenguji1 = penguji1.idUser
                JOIN pengguna penguji2 ON sidang.idPenguji2 = penguji2.idUser
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE (sidang.idPembimbing = ? OR sidang.idPenguji1 = ? OR sidang.idPenguji2 = ?)
                AND sidang.semesterTahunAkademik = ? AND sidang.sidangKe = ?
            `;
            

            conn.query(sql, [idDosen, idDosen, idDosen, thnakd, sk], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
                
            });
        }else{
            sql = `
                SELECT
                    sidang.idSidang as 'idSidang',
                    sidang.sidangKe as 'sidangKe',
                    sidang.semesterTahunAkademik as 'thnAkad', 
                    mhs.nomorInduk ,
                    mhs.nama as 'nama',
                    sidang.judul as 'judul',
                    sidang.fileBap as 'file'
                FROM sidang
                JOIN pengguna pembimbing ON sidang.idPembimbing = pembimbing.idUser
                JOIN pengguna penguji1 ON sidang.idPenguji1 = penguji1.idUser
                JOIN pengguna penguji2 ON sidang.idPenguji2 = penguji2.idUser
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE (sidang.idPembimbing = ? OR sidang.idPenguji1 = ? OR sidang.idPenguji2 = ?)
                AND sidang.semesterTahunAkademik = ?
            `;
        
                conn.query(sql, [idDosen, idDosen, idDosen, thnakd], (err, result) => {
                    if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
                
            });

        }
            conn.release();
        });
    }

    export const getBAP_Mhs = async (idMhs, thnakd, sk) => {
        const conn = await connDB();
        return new Promise((resolve, reject) => {
            let sql;
            if(sk == 1 || sk ==2){
                sql = `
                SELECT
                    sidang.idSidang as 'idSidang',
                    sidang.sidangKe as 'sidangKe',
                    sidang.semesterTahunAkademik as 'thnAkad', 
                    sidang.judul as 'judul',
                    sidang.fileBap as 'file'
                FROM sidang 
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser 
                WHERE sidang.idMhs = ? AND sidang.semesterTahunAkademik = ? AND sidang.sidangKe = ?`;
                
                conn.query(sql, [idMhs, thnakd, sk], (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                    
                });
            }else{
                sql = `
                SELECT
                    sidang.idSidang as 'idSidang',
                    sidang.sidangKe as 'sidangKe',
                    sidang.semesterTahunAkademik as 'thnAkad', 
                    sidang.judul as 'judul',
                    sidang.fileBap as 'file'
                FROM sidang 
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser 
                WHERE sidang.idMhs = ? AND sidang.semesterTahunAkademik = ?
                `;
            
                conn.query(sql, [idMhs, thnakd], (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                    
                });
            }
            conn.release();
        });
    };

    export const getSems = async () => {
        const conn = await connDB();
        return new Promise((resolve, reject) => {
            const sql = `
            SELECT DISTINCT
                semesterTahunAkademik
            FROM sidang`;
            conn.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
            conn.release();
        });
    }
    
    export const getSkrip = async (thnAkad) => {
        const conn = await connDB();
        return new Promise((resolve, reject) => {
            const sql = `
            SELECT DISTINCT
                sidangKe
            FROM sidang
            WHERE semesterTahunAkademik = ?`;
            conn.query(sql, [thnAkad], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
            conn.release();
        });
    }
    