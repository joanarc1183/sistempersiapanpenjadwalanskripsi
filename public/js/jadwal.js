const skripsiSelect = document.querySelectorAll("#skripsi-dropdown");
const semSelect = document.querySelectorAll("#semester-dropdown");
const tambah = document.querySelector("#add");
let details = document.querySelectorAll("td.detail");
const popUpTambah = document.querySelector("#addJadwal");
const popUpDetail = document.querySelector("#detailJadwal");

//tutup pop up
const batalTambah = document.querySelector("button#batal");
const tutupDetail = document.querySelector("button#tutup");

batalTambah.addEventListener('click', async () => {
    await reset();
    popUpTambah.style.display = "none";
})

tutupDetail.addEventListener('click', () => {
    popUpDetail.style.display = "none";
})

//menampilkan dropdown semester dan tahun akademik
async function getSem() {
    await delChild(semSelect[0])
    await delChild(semSelect[1])
    semSelect.forEach((sem) => {
        fetch("/getSem").then(onSuccess).then(showSem);

        function onSuccess(response) {
            return response.json();
        }

        function showSem(result) {
            for (let i = 0; i < result.length; i++) {
                const semOption = document.createElement("option");
                semOption.value = result[i][0];
                semOption.textContent = result[i][0];
                sem.appendChild(semOption);
            }
        }
    });
}

window.onload = function() {
    getSem();
}

semSelect[0].addEventListener('click', async () => {
    let thnAkad = semSelect[0].value;

    let sk;
    if (skripsiSelect[0].value == "Skripsi 1"){
        sk = 1
    } else if (skripsiSelect[0].value == "Skripsi 2"){
        sk = 2
    } else {
        sk = skripsiSelect[0].value
    }
    
    let data = {
        thn: thnAkad,
        sk: sk
    };
    const params = new URLSearchParams(data);
    const q = params.toString();
    const url = "/getSkripsi?" + q;
    fetch(url).then(onSuccess).then(getSkripsi);
    function onSuccess(response) {
        return response.json();
    }

    //menampilkan dropdown skripsi pada semester dan tahun akademik yang dipilih
    function getSkripsi(data) {
        const resultSkripsiOpt = data.skripsiO;
        const resultJadwal = data.jadwal;

        //kosongkan option
        let i = skripsiSelect[0].childElementCount;
        while(i > 1){
            skripsiSelect[0].removeChild(skripsiSelect[0].children[i-1])
            i--;
        }
        //tambahkan option
        for (let i = 0; i < resultSkripsiOpt.length; i++) {
            const skripsiOption = document.createElement("option");
            if(resultSkripsiOpt[i][0] == 1){
                skripsiOption.value = "Skripsi 1";
                skripsiOption.textContent = "Skripsi 1";
            }else{
                skripsiOption.value = "Skripsi 2";
                skripsiOption.textContent = "Skripsi 2";
            }
            skripsiSelect[0].appendChild(skripsiOption);
        }
        
        // untuk menampilkan tabel
        const tabel = document.querySelector("#jadwal tbody")
        // delete isi tabel bila sebelumnya ada tabel yang ditampilkan
        while (tabel.rows.length > 0){
            tabel.deleteRow(0);
        }

        for (let i=0; i<resultJadwal.length; i++){
            const tr = document.createElement("tr");

            const td1 = document.createElement("td");
            td1.textContent = (i+1);
            tr.appendChild(td1)

            const td2 = document.createElement("td");
            td2.textContent = resultJadwal[i][1];
            tr.appendChild(td2);

            const td3 = document.createElement("td");
            td3.textContent = resultJadwal[i][2];
            tr.appendChild(td3);
            
            const td4 = document.createElement("td");
            td4.textContent = resultJadwal[i][3];
            tr.appendChild(td4);
            
            const td5 = document.createElement("td");
            td5.textContent = resultJadwal[i][4];
            tr.appendChild(td5);

            const td6 = document.createElement("td");
            td6.textContent = resultJadwal[i][5];
            tr.appendChild(td6);

            const td7 = document.createElement("td");
            td7.textContent = resultJadwal[i][6];
            tr.appendChild(td7);

            const td8 = document.createElement("td");
            td8.textContent = resultJadwal[i][7];
            tr.appendChild(td8);

            const td9 = document.createElement("td");
            const img = document.createElement("img");
            img.src = "/assets/view-detail.png";
            td9.appendChild(img);
            td9.setAttribute("value", resultJadwal[i][0]);
            td9.classList.add("detail")
            tr.appendChild(td9);

            tabel.appendChild(tr);
        }

        // karena isi tabel berubah, akan meng-select bagian yang baru
        details = document.querySelectorAll("td.detail");

        // untuk klik tombol detail jadwal
        clickDetail()
    }
})

skripsiSelect[0].addEventListener('click', async () => {
    let thnAkad = semSelect[0].value;

    let sk;
    if (skripsiSelect[0].value == "Skripsi 1"){
        sk = 1
    } else if (skripsiSelect[0].value == "Skripsi 2"){
        sk = 2
    } else {
        sk = skripsiSelect[0].value
    }

    let data = {
        thn: thnAkad,
        sk: sk
    };
    const params = new URLSearchParams(data);
    const q = params.toString();
    const url = "/getSkripsi?" + q;
    fetch(url).then(onSuccess).then(getSkripsi);
    function onSuccess(response) {
        return response.json();
    }

    //menampilkan dropdown skripsi pada semester dan tahun akademik yang dipilih
    function getSkripsi(data) {
        const resultJadwal = data.jadwal;
        
        // untuk menampilkan tabel
        const tabel = document.querySelector("#jadwal tbody")
        // delete isi tabel bila sebelumnya ada tabel yang ditampilkan
        while (tabel.rows.length > 0){
            tabel.deleteRow(0);
        }

        for (let i=0; i<resultJadwal.length; i++){
            const tr = document.createElement("tr");

            const td1 = document.createElement("td");
            td1.textContent = (i+1);
            tr.appendChild(td1)

            const td2 = document.createElement("td");
            td2.textContent = resultJadwal[i][1];
            tr.appendChild(td2);

            const td3 = document.createElement("td");
            td3.textContent = resultJadwal[i][2];
            tr.appendChild(td3);
            
            const td4 = document.createElement("td");
            td4.textContent = resultJadwal[i][3];
            tr.appendChild(td4);
            
            const td5 = document.createElement("td");
            td5.textContent = resultJadwal[i][4];
            tr.appendChild(td5);

            const td6 = document.createElement("td");
            td6.textContent = resultJadwal[i][5];
            tr.appendChild(td6);

            const td7 = document.createElement("td");
            td7.textContent = resultJadwal[i][6];
            tr.appendChild(td7);

            const td8 = document.createElement("td");
            td8.textContent = resultJadwal[i][7];
            tr.appendChild(td8);

            const td9 = document.createElement("td");
            const img = document.createElement("img");
            img.src = "/assets/view-detail.png";
            td9.appendChild(img);
            td9.setAttribute("value", resultJadwal[i][0]);
            td9.setAttribute("class", "detail");
            tr.appendChild(td9);

            tabel.appendChild(tr);
        }
        // karena isi tabel berubah, akan meng-select bagian yang baru
        details = document.querySelectorAll("td.detail");

        // untuk klik tombol detail jadwal
        clickDetail()
    }
})

//show pop up detail jadwal ---------------------------------------------------------------------------------------------------------
const isiDetails = document.querySelectorAll("#detail p.isi");

function clickDetail(){
    details.forEach(function (curDetail) {
        curDetail.addEventListener('click', () => {
            let line = curDetail.attributes.value.value;
            let data = {
                cur: line,
            };
            const params = new URLSearchParams(data);
            const q = params.toString();
            const url = "/getDetail?" + q;
            fetch(url).then(onSuccess).then(showPopUp);
            function onSuccess(response) {
                return response.json();
            }
            function showPopUp(result) {
                for(let i = 0; i<result[0].length; i++){
                    isiDetails[i].textContent = result[0][i];
                }
                popUpDetail.style.display = "flex";
            }
        })
    })
}

//show pop up tambah jadwal ---------------------------------------------------------------------------------------------------------
const allInput = document.querySelectorAll("#inputFrom .input");
const tanggalInput = document.querySelector("input[name='tanggalSidang']");
const waktuInput = document.querySelector("input[name='waktuSidang']");
const ruangSelect = document.querySelector("#ruangSidang-dropdown");
const npmMhsSelect = document.querySelector("#npmMhs-dropdown");
const namaMhsInput = document.querySelector("input[name='namaMhs']");
const judulInput = document.querySelector("input[name='judulSkripsi']");
const pbuSelect = document.querySelector("#pbu-dropdown");
const pu1Select = document.querySelector("#pu1-dropdown");
const pu2Select = document.querySelector("#pu2-dropdown");
const submitButton = document.querySelector("button#unggah");

function filled(){
    if(submitButton.disabled){
        submitButton.style.opacity = "0.5";
        submitButton.style.cursor = "default";
    }else{
        submitButton.style.opacity = "1";
        submitButton.style.cursor = "pointer";
    }
}

async function semSkripsi(sem, skripsiKe) {
    let data = {
        semAdd: sem,
        skripsiAdd: skripsiKe
    };
    const params = new URLSearchParams(data);
    const q = params.toString();
    const url = "/semSkripsiAdd?" + q;
    fetch(url).then(onSuccess);
    function onSuccess(response) {
        return response.json();
    }
}

async function getRuang() {
    fetch("/getRuangAdd").then(onSuccess).then(showRuang);

    function onSuccess(response) {
        return response.json();
    }

    function showRuang(result) {
        delChild(ruangSelect)
        for (let i = 0; i < result.length; i++) {
            const ruangOption = document.createElement("option");
            ruangOption.value = result[i][0];
            ruangOption.textContent = result[i][0];
            ruangSelect.appendChild(ruangOption);
        }
    }
}

let mhsSidang;
async function getNpmMhs() {
    fetch("/getMhsAdd").then(onSuccess).then(showRuang);

    function onSuccess(response) {
        return response.json();
    }

    function showRuang(result) {
        delChild(npmMhsSelect)
        mhsSidang = result;
        for (let i = 0; i < result.length; i++) {
            const npmOption = document.createElement("option");
            npmOption.value = result[i][2];
            npmOption.textContent = result[i][1];
            npmMhsSelect.appendChild(npmOption);
        }
    }
}

function getNamaMhs() {
    if(npmMhsSelect.value != ""){
        for (let i = 0; i < mhsSidang.length; i++) {
            if (mhsSidang[i][2] == npmMhsSelect.value){
                namaMhsInput.value = mhsSidang[i][0];
            }
        }
    }
}

let pbu, pu1, pu2;
async function getPbu() {
    fetch("/getDosenAdd").then(onSuccess).then(showPbu);
    
    function onSuccess(response) {
        return response.json();
    }
    
    function showPbu(result) {
        delChild(pbuSelect)
        for (let i = 0; i < result.length; i++) {
            if(result[i][2] != pu2 && result[i][2] != pu1){
                const pbuOption = document.createElement("option");
                pbuOption.value = result[i][2];
                pbuOption.textContent = result[i][1]+" - "+result[i][0];
                pbuSelect.appendChild(pbuOption);
            }
        }
    }
}

async function getPu1() {
    fetch("/getDosenAdd").then(onSuccess).then(showPu1);

    function onSuccess(response) {
        return response.json();
    }

    function showPu1(result) {
        delChild(pu1Select)
        for (let i = 0; i < result.length; i++) {
            if(result[i][2] != pbu && result[i][2] != pu2){
                const pu1Option = document.createElement("option");
                pu1Option.value = result[i][2];
                pu1Option.textContent = result[i][1]+" - "+result[i][0];
                pu1Select.appendChild(pu1Option);
            }
        }
    }
}

async function getPu2() {
    fetch("/getDosenAdd").then(onSuccess).then(showPu2);

    function onSuccess(response) {
        return response.json();
    }

    function showPu2(result) {
        delChild(pu2Select)
        for (let i = 0; i < result.length; i++) {
            if(result[i][2] != pbu && result[i][2] != pu1){
                const pu2Option = document.createElement("option");
                pu2Option.value = result[i][2];
                pu2Option.textContent = result[i][1]+" - "+result[i][0];
                pu2Select.appendChild(pu2Option);
            }
        }
    }
}

let valid = true;
tambah.addEventListener('click', async () => {
    popUpTambah.style.display = "flex";
    await getSem().then(filled);

    semSelect[1].addEventListener('click', () => {
        skripsiSelect[1].removeAttribute('disabled');
        skripsiSelect[1].addEventListener('click', async () => {
            await semSkripsi(semSelect[1].value, skripsiSelect[1].value);
            tanggalInput.removeAttribute('disabled');
            tanggalInput.addEventListener('input', () => {
                waktuInput.removeAttribute('disabled');
                waktuInput.addEventListener('input', async() => {
                    await getRuang();
                    ruangSelect.removeAttribute('disabled');
                    ruangSelect.addEventListener('click', async() => {
                        await getNpmMhs();
                        npmMhsSelect.removeAttribute('disabled');
                        npmMhsSelect.addEventListener('click', async() => {
                            getNamaMhs();
                            judulInput.removeAttribute('disabled');
                            judulInput.addEventListener('input', async() => {
                                await getPbu();
                                await checkInput();
                                filled();
                                pbuSelect.removeAttribute('disabled');
                                pbuSelect.addEventListener('click', async() => {
                                    pbu = pbuSelect.value;
                                    await getPu1();
                                    pu1Select.removeAttribute('disabled');
                                    pu1Select.addEventListener('click', async() => {
                                        pu1 = pu1Select.value;
                                        await getPu2();
                                        pu2Select.removeAttribute('disabled');
                                        pu2Select.addEventListener('click', async() => {
                                            pu2 = pu2Select.value;
                                            await checkInput();
                                            submitButton.removeAttribute('disabled');
                                            filled();
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})

//submit tambah jadwal
submitButton.addEventListener('click', async() => {
    await checkInput();
    if(valid){
        alert("Berhasil tambah jadwal sidang!");
        popUpTambah.style.display = "none";
    }else{
        alert("Gagal tambah jadwal sidang, semua input wajib diisi!");
    }
})

async function checkInput(){
    allInput.forEach((inputSpace)=>{
        if(inputSpace.value==""){
            valid = false;
            submitButton.setAttribute('disabled');
        }
    })
}
  
//close pop up ----------------------------------------------------------------------------------------------------------------------
const inputSpace = document.querySelectorAll("#inputForm .input");
async function delChild(parentEl) {
    while(parentEl.firstChild){
        parentEl.removeChild(parentEl.firstChild)
    }
}
async function reset() {
    inputSpace.forEach((space) => {
        space.value = ""
    });
    delChild(semSelect[1])
    delChild(ruangSelect)
    delChild(npmMhsSelect)
    delChild(pbuSelect)
    delChild(pu1Select)
    delChild(pu2Select)
}

window.onclick = async function(event) {
    if (event.target == popUpTambah) {
        await reset();
        popUpTambah.style.display = "none";
    }
}
window.onclick = function(event) {
    if (event.target == popUpDetail) {
        popUpDetail.style.display = "none";
    }
}
