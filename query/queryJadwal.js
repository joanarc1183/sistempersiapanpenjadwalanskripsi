import { connDB } from './connectionDatabase.js';

//show jadwal
export const getJadwalKoord = async (thnakd, sk) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
        if(sk == 1 || sk == 2){
            sql = `
                SELECT
                    sidang.idSidang,
                    sidang.sidangKe,
                    DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
                    TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
                    sidang.ruang,
                    mhs.nomorInduk AS 'npmMhs',
                    mhs.nama AS 'namaMhs',
                    sidang.judul
                FROM sidang 
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE sidang.semesterTahunAkademik = ? AND sidang.sidangKe = ?
            `;
            conn.query(sql, [thnakd, sk], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        } else {
            sql = `
                SELECT
                    sidang.idSidang,
                    sidang.sidangKe,
                    DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
                    TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
                    sidang.ruang,
                    mhs.nomorInduk AS 'npmMhs',
                    mhs.nama AS 'namaMhs',
                    sidang.judul
                FROM sidang 
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE sidang.semesterTahunAkademik = ?
            `;
            conn.query(sql, [thnakd], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        }
        conn.release();
        
    });
}

export const getJadwalDosen = async (thnakd, sk, idDosen) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
        if(sk == 1 || sk == 2){
            sql = `
                SELECT
                    sidang.idSidang,
                    sidang.sidangKe,
                    DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
                    TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
                    sidang.ruang,
                    mhs.nomorInduk AS 'npmMhs',
                    mhs.nama AS 'namaMhs',
                    sidang.judul
                FROM sidang
                JOIN pengguna pembimbing ON sidang.idPembimbing = pembimbing.idUser
                JOIN pengguna penguji1 ON sidang.idPenguji1 = penguji1.idUser
                JOIN pengguna penguji2 ON sidang.idPenguji2 = penguji2.idUser
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE (sidang.idPembimbing = ? OR sidang.idPenguji1 = ? OR sidang.idPenguji2 = ?)
                AND sidang.semesterTahunAkademik = ? AND sidang.sidangKe = ?
            `;
            conn.query(sql, [idDosen, idDosen, idDosen, thnakd, sk], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        } else {
            sql = `
                SELECT
                    sidang.idSidang,
                    sidang.sidangKe,
                    DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
                    TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
                    sidang.ruang,
                    mhs.nomorInduk AS 'npmMhs',
                    mhs.nama AS 'namaMhs',
                    sidang.judul
                FROM sidang
                JOIN pengguna pembimbing ON sidang.idPembimbing = pembimbing.idUser
                JOIN pengguna penguji1 ON sidang.idPenguji1 = penguji1.idUser
                JOIN pengguna penguji2 ON sidang.idPenguji2 = penguji2.idUser
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
                WHERE (sidang.idPembimbing = ? OR sidang.idPenguji1 = ? OR sidang.idPenguji2 = ?)
                AND sidang.semesterTahunAkademik = ?
            `;
            conn.query(sql, [idDosen, idDosen, idDosen, thnakd], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        }
        
        conn.release();
    });
}

export const getJadwalMhs = async (thnakd, sk, idMhs) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        let sql;
        if(sk == 1 || sk == 2){
            sql = `
                SELECT
                    sidang.idSidang,
                    sidang.sidangKe,
                    DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
                    TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
                    sidang.ruang,
                    mhs.nomorInduk AS 'npmMhs',
                    mhs.nama AS 'namaMhs',
                    sidang.judul
                FROM sidang 
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser 
                WHERE sidang.semesterTahunAkademik = ? AND sidang.sidangKe = ? AND sidang.idMhs = ?
            `;
            conn.query(sql, [thnakd, sk, idMhs], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        } else {
            sql = `
                SELECT
                    sidang.idSidang,
                    sidang.sidangKe,
                    DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
                    TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
                    sidang.ruang,
                    mhs.nomorInduk AS 'npmMhs',
                    mhs.nama AS 'namaMhs',
                    sidang.judul
                FROM sidang 
                JOIN pengguna mhs ON sidang.idMhs = mhs.idUser 
                WHERE sidang.semesterTahunAkademik = ? AND sidang.idMhs = ?
            `;
            conn.query(sql, [thnakd, idMhs], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        }
        conn.release();
    });
}

//detail jadwal
export const getDetail = async (idSidang) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT 
            sidang.sidangKe,
            sidang.semesterTahunAkademik AS 'thnAkad',
            DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal',
            TIME_FORMAT(waktu, '%H:%i') AS 'waktu',
            sidang.ruang,
            mhs.nomorInduk AS 'npmMhs',
            mhs.nama AS 'namaMhs',
            sidang.judul,
            pembimbing.nama AS 'namaPembimbing',
            penguji1.nama AS 'namaPenguji1',
            penguji2.nama AS 'namaPenguji2'
            FROM sidang 
        JOIN pengguna mhs ON sidang.idMhs = mhs.idUser
        JOIN pengguna pembimbing ON sidang.idPembimbing = pembimbing.idUser
        JOIN pengguna penguji1 ON sidang.idPenguji1 = penguji1.idUser
        JOIN pengguna penguji2 ON sidang.idPenguji2 = penguji2.idUser
        WHERE sidang.idSidang = ?`;
        conn.query(sql, idSidang, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getSem = async () => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT DISTINCT
            semesterTahunAkademik
        FROM sidang`;
        conn.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getSkripsi = async (thnAkad) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT DISTINCT
            sidangKe
        FROM sidang
        WHERE semesterTahunAkademik = ?`;
        conn.query(sql, [thnAkad], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

//tambah jadwal
export const getRuangAdd = async () => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT DISTINCT
            ruang
        FROM sidang`;
        conn.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getMhsAdd = async (sem, skripsi) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT 
            pengguna.nama AS "namaMhs",
            pengguna.nomorInduk AS "npmMhs",
            pengguna.idUser AS "idMhs"
        FROM pengguna
        LEFT JOIN sidang ON pengguna.idUser = sidang.idMhs
            AND sidang.semesterTahunAkademik = ?
            AND sidang.sidangKe = ?
        WHERE sidang.idMhs IS NULL AND pengguna.status = 0;`;
        conn.query(sql, [sem, skripsi], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getDosenAdd = async () => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT 
            pengguna.nama AS "namaDos",
	        pengguna.nomorInduk AS "npmDos",
            pengguna.idUser AS "idDos"
   	    FROM pengguna
        WHERE pengguna.status = 1`;
        conn.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const getCurId = async () => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT idSidang FROM sidang ORDER BY idSidang DESC LIMIT 1;';
        conn.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const insertNilai1 = async (idSidang, idBobot) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO Nilai1 (idSidang, idBobot, nilai) VALUES (?, ?, 0);';
        conn.query(sql, [idSidang, idBobot], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const insertNilai2 = async (idSidang, idBobot) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO Nilai2 (idSidang, idBobot, nilai) VALUES (?, ?, 0);';
        conn.query(sql, [idSidang, idBobot], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const insertSidang = async (addSidangKe, addThnAkad, addTanggal, addWaktu, addRuang, addJudul, addIdMhs, addIdPbu, addIdPu1, addIdPu2) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO Sidang (sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, fileBap, statusTandaTangan, idMhs, idPembimbing, idPenguji1, idPenguji2) VALUES (?, ?, ?, ?, ?, ?, "", "", 0, ?, ?, ?, ?);';
        conn.query(sql, [addSidangKe, addThnAkad, addTanggal, addWaktu, addRuang, addJudul, addIdMhs, addIdPbu, addIdPu1, addIdPu2], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

