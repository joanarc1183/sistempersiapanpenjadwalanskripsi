import mysql from 'mysql';

export const pool = mysql.createPool({
    user: 'root',
    password: '',
    database: 'sistem_persiapan_penjadwalan_skripsi',
    host: 'localhost'
});

export const connDB = ()=>{
    return new Promise((resolve, reject) => {
        pool.getConnection((err, conn) => {
            if (err) {
                reject(err);
            } else {
                resolve(conn);
            }
        });
    });
};