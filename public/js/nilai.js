const semesterDropdown = document.getElementById('semesterDropdown');
const skripsiDropdown = document.getElementById('skripsiDropdown');
const npmDropdown = document.getElementById('npmDropdown');
const tableNilai = document.getElementById('tableNilai');
const tableNilai2 = document.getElementById('tableNilai2');
const cari = document.getElementById("cari");

function valid(){
  if (semesterDropdown.value != "" && skripsiDropdown.value != ""){
    cari.removeAttribute('disabled');
    cari.style.opacity = "1";
  } else {
    cari.setAttribute('disabled', true);
    cari.style.opacity = "0.5";
  }
  if (semesterDropdown.value != "" && skripsiDropdown.value != "" && npmDropdown.value != ""){
    cari.removeAttribute('disabled');
    cari.style.opacity = "1";
  } else {
    cari.setAttribute('disabled', true);
    cari.style.opacity = "0.5";
  }
}

semesterDropdown.addEventListener('click', function(){
  const get_semesterDropdown = semesterDropdown.value;
  const data = {
    thnAkad: get_semesterDropdown
  };
  const params = new URLSearchParams(data);
  const q = params.toString();
  const url = "/getSidangKeNilai?" + q;

  // kirim data tahun akademik ke server
  fetch(url).then(onSuccess).then(showResult);
  function onSuccess(response){
    return response.json();
  }
  function showResult(resultSidangKe){
    skripsiDropdown.innerHTML='';
    // untuk setiap sidang ke berapa
    for(let i of resultSidangKe){
      // kalau dia(npm) tidak ada di tahun akademik sekian dan skripsi ke sekian
      const data1 = {
        thnAkad: get_semesterDropdown,
        sdgKe: i
      };
      const params1 = new URLSearchParams(data1);
      const r = params1.toString();
      const url1 = "/getNPMNilai?" + r;
      
      // kirim data tahun akademik dan skripsi ke-
      fetch(url1).then(onSuccess1).then(showResult1);
      // eslint-disable-next-line no-inner-declarations
      function onSuccess1(response1){
        return response1.json();
      }
      // eslint-disable-next-line no-inner-declarations
      function showResult1(resultSkripsiKe){
        // bila ada npm yang tersedia di tahun akademik sekian dan skripsi ke sekian
        if (resultSkripsiKe.length != 0) {
          // buat option isinya skripsi keberapa
          const option = document.createElement('option');
          option.value = i;
          option.textContent = i;
          skripsiDropdown.appendChild(option);
        }
      }
    }
    skripsiDropdown.removeAttribute('disabled');
  }
  valid();
});

skripsiDropdown.addEventListener('click', () => {
    const getSkripsi = skripsiDropdown.value;
    const getSemester = semesterDropdown.value;
    const data = {
        thnAkad: getSemester,
        sdgKe: getSkripsi
    };
    
    const params = new URLSearchParams(data);
    const q = params.toString();
    const url = "/getNPMNilai?" + q;
    
    fetch(url).then(onSuccess).then(showResult);
    function onSuccess(response){
        return response.json();
    }

    function showResult(resultNPM){
        if (resultNPM.length != 0){
          npmDropdown.innerHTML='';
          for(let i of resultNPM){
            const option = document.createElement('option');
            option.value = i[9];
            option.textContent = i[9];
            npmDropdown.appendChild(option);
          }
          npmDropdown.removeAttribute('disabled');
        } else {
            npmDropdown.setAttribute('disabled', true);
            npmDropdown.innerHTML='';
        }
      }
    valid();
    if(getSkripsi == 1){
        tableNilai.style.display = 'block';
        tableNilai2.style.display = 'none';
    }else{
        tableNilai.style.display = 'none';
        tableNilai2.style.display = 'block';
    }
})

npmDropdown.addEventListener('click', function(){
    valid();
});


//Nilai

const submitButton = document.getElementById('submitButton');
const cancelButton = document.getElementById('cancelButton');
const inputNilai = document.querySelectorAll("input.inputNilai");

// tombol submit dianggap tidak bisa di klik
submitButton.disabled = true;
function checkInput() {
  inputNilai.forEach(inputN => {
    if (inputN.value == 0){
      // bila masih ada input yang harus diisi
      // tombol submit bisa di klik
      // inputN.disabled = false;
      submitButton.disabled = false;
      submitButton.style.opacity = "1"; 
      submitButton.style.cursor = "pointer";
    } else {
      // bila tidak ada input yang diisi
      // tombol tetap tidak bisa di klik
      // inputN.disabled = true;
      submitButton.style.opacity = "0.5"; 
      submitButton.style.cursor = "default";
    }
  });
}
checkInput();

submitButton.addEventListener("click", function(){
  let arrN = [];
  inputNilai.forEach(inputN => {
    const inn = inputN.value+""
    arrN.push(inn);
  });
  
  const data1 = {
    arrN: arrN
  }
  const params = new URLSearchParams(data1);
  const q = params.toString();
  const url1 = "/uploadNilai?" + q;

  // Menonaktifkan tombol submit
  submitButton.disabled = true;
  // Mengubah opacity tombol submit
  submitButton.style.opacity = 0.5;

  // Menonaktifkan tombol cancel
  cancelButton.disabled = true;
  // Mengubah opacity tombol cancel
  cancelButton.style.opacity = 0.5;

  fetch(url1).then(onSuccess1);
  function onSuccess1(response){
    return response.json();
  }

  checkInput();

  alert("Berhasil upload nilai!");
});