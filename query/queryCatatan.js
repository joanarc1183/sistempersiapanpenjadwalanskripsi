import { connDB } from './connectionDatabase.js';

export const catatanPerUser = async (idUser) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT DISTINCT semesterTahunAkademik FROM `sidang` WHERE idMhs = ? || idPembimbing = ? || idPenguji1 = ? || idPenguji2 = ?';
        conn.query(sql, [idUser, idUser, idUser, idUser], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const get_sidangKe = async (thnAkd) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = 'SELECT DISTINCT sidangKe FROM `sidang` WHERE semesterTahunAkademik = ?';
        conn.query(sql, thnAkd, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

export const get_npm = async (nomorInduk, thnAkd, sdgKe) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
            SELECT idSidang, sidangKe, semesterTahunAkademik, DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal', TIME_FORMAT(waktu, '%H:%i') AS 'waktu', ruang, judul, isiCatatan, TablePeng1.status, TablePeng1.nomorInduk, namaMhs, idPembimbing, namaPembimbing, NIKPembimbing, namaPenguji1, NIKPenguji1, pengguna.nama AS namaPenguji2, pengguna.nomorInduk AS NIKPenguji2
            FROM 
            (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TablePemb.status, TablePemb.nomorInduk, namaMhs, idPembimbing, namaPembimbing, NIKPembimbing, idPenguji2, pengguna.nama AS namaPenguji1, pengguna.nomorInduk AS NIKPenguji1
                FROM 
                (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TableMhs.status, TableMhs.nomorInduk, namaMhs, idPembimbing, idPenguji1, idPenguji2, pengguna.nama AS namaPembimbing, pengguna.nomorInduk AS NIKPembimbing
                    FROM 
                    (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, pengguna.status, pengguna.nomorInduk, nama as "namaMhs", idPembimbing, idPenguji1, idPenguji2
                        FROM sidang 
                            JOIN pengguna ON sidang.idMhs = pengguna.idUser) AS TableMhs
                    JOIN pengguna ON TableMhs.idPembimbing = pengguna.idUser) AS TablePemb
                JOIN pengguna ON TablePemb.idPenguji1 = pengguna.idUser) AS TablePeng1
            JOIN pengguna ON TablePeng1.idPenguji2 = pengguna.idUser
            WHERE (TablePeng1.nomorInduk = ? || NIKPembimbing = ? || NIKPenguji1 = ? || Pengguna.nomorInduk = ?) && semesterTahunAkademik = ? && sidangKe = ? 
        `;
        conn.query(sql, [nomorInduk, nomorInduk, nomorInduk, nomorInduk, thnAkd, sdgKe], (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
        conn.release();
    });
}

let hasilCat;
export const catatan = async (thnAkd, sdgKe, npmMhs) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql = `
            SELECT idSidang, sidangKe, semesterTahunAkademik, DATE_FORMAT(tanggal, '%Y-%m-%d') AS 'tanggal', TIME_FORMAT(waktu, '%H:%i') AS 'waktu', ruang, judul, isiCatatan, TablePeng1.nomorInduk, namaMhs, idPembimbing, namaPembimbing, namaPenguji1, pengguna.nama AS namaPenguji2
                FROM 
                (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TablePemb.nomorInduk, namaMhs, idPembimbing, namaPembimbing, idPenguji2, pengguna.nama AS namaPenguji1
                    FROM 
                    (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, TableMhs.nomorInduk, namaMhs, idPembimbing, idPenguji1, idPenguji2, pengguna.nama AS namaPembimbing
                        FROM 
                        (SELECT idSidang, sidangKe, semesterTahunAkademik, tanggal, waktu, ruang, judul, isiCatatan, pengguna.nomorInduk, nama as "namaMhs", idPembimbing, idPenguji1, idPenguji2
                            FROM sidang 
                            JOIN pengguna ON sidang.idMhs = pengguna.idUser 
                            WHERE semesterTahunAkademik = ? && sidangKe = ? && nomorInduk = ?) AS TableMhs
                    JOIN pengguna ON TableMhs.idPembimbing = pengguna.idUser) AS TablePemb
                JOIN pengguna ON TablePemb.idPenguji1 = pengguna.idUser) AS TablePeng1
            JOIN pengguna ON TablePeng1.idPenguji2 = pengguna.idUser
        `;
        conn.query(sql, [thnAkd, sdgKe, npmMhs], (err, result) => {
            if (err) {
                reject(err);
            } else {
                hasilCat = result;
                resolve(result);
            }
        });
        conn.release();
    });
}

export const uploadCatatan = async (isiCat) => {
    const conn = await connDB();
    return new Promise((resolve, reject) => {
        const sql1 = `
            UPDATE sidang
            SET isiCatatan = ?
            WHERE idSidang = ?; 
            `;
            conn.query(sql1, [isiCat, hasilCat[0].idSidang], (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    result = true
                    resolve(result);
            }
        });
        conn.release();
    });
}