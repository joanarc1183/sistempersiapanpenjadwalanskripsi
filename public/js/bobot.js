const skripsi = document.querySelector("#skripsi-dropdown");
const semester = document.querySelector("#semester-dropdown");
const tab1 = document.querySelector("table#table1");
const tab2 = document.querySelector("table#table2");
const tabn = document.querySelector("#tableNone");
const tab1body = document.querySelector("table#table1 tbody");
const tab2body = document.querySelector("table#table2 tbody");
const buttonContainer = document.querySelector(".buttonContainer");

let data;
async function showTable(dataQuery) {

    const params = new URLSearchParams(dataQuery);
    const q = params.toString();
    const url = `/getBobot?${q}`;

    fetch(url).then(onSuccess).then(showTable).then(update).then(tombol);
    function onSuccess(response){
        return response.json();
    }
    function showTable(result) {
        let curTab, curRow, curCell;
        let sum = 0;
        while(tab1body.firstChild){
            tab1body.removeChild(tab1body.firstChild)
        }
        while(tab2body.firstChild){
            tab2body.removeChild(tab2body.firstChild)
        }
        if(getSkripsi==1){
            curTab = tab1body;
        }else{
            curTab = tab2body;
        }
        for (let i = 0; i < result.length; i++) {
            //buat baris
            curRow = document.createElement("tr");
            curRow.id = result[i][0];
            //kolom idKomponen/idPeran
            curCell = document.createElement("td");
            curCell.id = 'id'+result[i][0];
            curCell.classList.add("idKomp")
            curCell.textContent = result[i][0];
            curRow.appendChild(curCell);
            if(getSkripsi==1){
                //kolom nama komponen untuk skripsi 1 (bisa diedit)
                curCell = document.createElement("td");
                curCell.id = result[i][1];
                const inNama = document.createElement("input");
                inNama.type = 'text'
                inNama.id = 'nama'+result[i][0]
                inNama.classList.add('namaKomp')
                inNama.value = result[i][1];
                curCell.appendChild(inNama);
            }else{
                //kolom nama peran untuk skripsi 2 (tidak bisa diedit)
                curCell = document.createElement("td");
                curCell.id = 'nama'+result[i][0];
                curCell.classList.add('namaKomp')
                curCell.textContent = result[i][1];
            }
            curRow.appendChild(curCell);
            //kolom bobot (bisa diedit)
            curCell = document.createElement("td");
            curCell.id = result[i][2];
            const inBobot = document.createElement("input");
            inBobot.type = 'Number'
            inBobot.id = 'bobot'+result[i][0]
            inBobot.classList.add('bobotKomp')
            inBobot.value = result[i][2];
            sum += result[i][2];
            curCell.appendChild(inBobot);
            curRow.appendChild(curCell);
            //append baris ke tabel
            curTab.appendChild(curRow);
        }
        //buat baris total
        curRow = document.createElement("tr");
        curRow.id = "totalRow";
        curCell = document.createElement("td");
        curCell.id = "noKosongBobotNilai";
        curRow.appendChild(curCell);
        curCell = document.createElement("td");
        curCell.id = "totalBobotNilai";
        curCell.textContent = "Total";
        curRow.appendChild(curCell);
        curCell = document.createElement("td");
        curCell.id = "total";
        curCell.textContent = sum;
        curRow.appendChild(curCell);
        //append baris ke tabel
        curTab.appendChild(curRow);
    }
    function update(){
        //change bobot komponen
        const changeBobot = document.querySelectorAll("input.bobotKomp");
        const totalRow = document.querySelector("tr#totalRow");
        
        //untuk setiap bobot
        changeBobot.forEach((curChange) => {
            //setiap kali bobot diubah
            curChange.addEventListener("input", async function(){
                let curTotal = 0;
                await reSum();          //hitung ulang total bobot
                await updateTotal();    //tampilkan total bobot
                async function reSum(){
                    changeBobot.forEach((curValue) => {
                        curTotal+=parseFloat(curValue.value);
                    })
                }
                async function updateTotal(){
                    let updateTotal = document.createElement("td");
                    updateTotal.id = "total";
                    updateTotal.textContent = curTotal;
                    let lastChild = totalRow.childElementCount-1;
                    totalRow.removeChild(totalRow.children[lastChild]);
                    totalRow.appendChild(updateTotal);
                }
            })
        })
    }
    function tombol(){
        buttonContainer.style.display = "block";
    }
}

let getSem, getSkripsi;
semester.addEventListener('click', async function(){
    getSem = semester.value;
    if(skripsi.value=="skripsi-1"){
        getSkripsi = 1;
    }else{
        getSkripsi = 2;
    }
    data = {
        sem: getSem,
        skripsi: getSkripsi,
    };
    
    await showTable(data, getSkripsi);

    if(getSkripsi == 1){
        tab1.style.display = 'block';
        tab2.style.display = 'none';
        tabn.style.display = 'none';
    }else{
        tab1.style.display = 'none';
        tab2.style.display = 'block';
        tabn.style.display = 'none';
    }
})

skripsi.addEventListener('input', async () => {
    getSem = semester.value;
    if(skripsi.value=="skripsi-1"){
        getSkripsi = 1;
    }else{
        getSkripsi = 2;
    }
    data = {
        sem: getSem,
        skripsi: getSkripsi,
    };

    await showTable(data, getSkripsi);

    if(getSkripsi == 1){
        tab1.style.display = 'block';
        tab2.style.display = 'none';
        tabn.style.display = 'none';
    }else{
        tab1.style.display = 'none';
        tab2.style.display = 'block';
        tabn.style.display = 'none';
    }
})

//update bobot
const cancel = document.querySelector("button#cancelButton");
const submit = document.querySelector("button#submitButton");

//batal ubah bobot
cancel.addEventListener("click", async function(){
    await showTable(data, getSkripsi);
    
    if(getSkripsi == 1){
        tab1.style.display = 'block';
        tab2.style.display = 'none';
        tabn.style.display = 'none';
    }else{
        tab1.style.display = 'none';
        tab2.style.display = 'block';
        tabn.style.display = 'none';
    }
})

//submit perubahan bobot
submit.addEventListener("click", async function(){
    const idKomp = document.querySelectorAll(".idKomp")
    const namaKomp = document.querySelectorAll(".namaKomp")
    const bobotKomp = document.querySelectorAll(".bobotKomp")
    const totalBobot = document.querySelector("td#total")
    let namaFilled = false;
    await cekNama();

    let bobotFilled = false;
    await cekBobot();

    //periksa apabila ada nama komponen yang kosong 
    async function cekNama(){
        namaKomp.forEach((nama)=>{
            if(nama.value==""){
                namaFilled = true
            }
        })
    }
    
    //periksa apabila ada bobot yang kosong
    async function cekBobot(){
        bobotKomp.forEach((bobot)=>{
            if(bobot.value==""){
                bobotFilled = true
            }
        })
    }
    
    if(getSkripsi == 1 && namaFilled){
        alert("Nama Komponen Tidak Boleh Kosong")
    }else if(bobotFilled){
        alert("Bobot Komponen Tidak Boleh Kosong")
    }else if(totalBobot.textContent != 100){
        alert("Total Bobot Harus 100")
    }else{  //update tabel bobot pada database
        let keys = []
        let values = []
        let idx = 0
        idKomp.forEach((id)=>{
            keys[idx] = id.id
            values[idx] = id.textContent
            idx++
        })
        namaKomp.forEach((nama)=>{
            keys[idx] = nama.id
            if(getSkripsi==1){
                values[idx] = nama.value
            }else{
                values[idx] = nama.textContent
            }
            idx++
        })
        bobotKomp.forEach((bobot)=>{
            keys[idx] = bobot.id
            values[idx] = bobot.value
            idx++
        })
        keys[idx] = "sem"
        values[idx] = getSem
        idx++
        keys[idx] = "skripsi"
        values[idx] = getSkripsi

        let obj = {}
        for (let i = 0; i < keys.length; i++) {
            obj[keys[i]] = values[i];
        }
        let init = {
            method: 'post',
            headers:{
                "Content-Type":"application/json"
            },
            body: JSON.stringify(obj)
        };
        fetch('/updateBobot',init).then(onSuccess);
        alert("Berhasil Update Bobot")
    }
    function onSuccess(response){
        return response.json();
    }
})