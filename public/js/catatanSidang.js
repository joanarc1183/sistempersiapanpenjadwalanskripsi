const thnAkad = document.querySelector("select[name='thnAkad']");
const sidangKe = document.querySelector("select[name='sidangKe']");
const npm = document.querySelector("select[name='npmMhs']");
const cari = document.getElementById("cari");

function valid(){
  // tombol cari tidak bisa ditekan sebelum semua value dipilih/terisi
  // untuk mahasiswa
  if (thnAkad.value != "" && sidangKe.value != ""){
    cari.removeAttribute('disabled');
    cari.style.opacity = "1";
  } else {
    cari.setAttribute('disabled', true);
    cari.style.opacity = "0.5";
  }
  // untuk dosen
  if (thnAkad.value != "" && sidangKe.value != "" && npm.value != ""){
    cari.removeAttribute('disabled');
    cari.style.opacity = "1";
  } else {
    cari.setAttribute('disabled', true);
    cari.style.opacity = "0.5";
  }
}

thnAkad.addEventListener('click', function(){
  const get_thnAkad = thnAkad.value;
  const data = {
    thnAkad: get_thnAkad
  };
  const params = new URLSearchParams(data);
  const q = params.toString();
  const url = "/getSidangKe?" + q;
  
  // kirim data tahun akademik ke server
  fetch(url).then(onSuccess).then(showResult);
  function onSuccess(response){
    return response.json();
  };
  function showResult(resultSidangKe){
    sidangKe.innerHTML='';
    // untuk setiap sidang ke berapa
    for(let i of resultSidangKe){
      // kalau dia(npm) tidak ada di tahun akademik sekian dan skripsi ke sekian
      const data1 = {
        thnAkad: get_thnAkad,
        sdgKe: i
      };
      const params1 = new URLSearchParams(data1);
      const r = params1.toString();
      const url1 = "/getNPM?" + r;
      
      // kirim data tahun akademik dan skripsi ke-
      fetch(url1).then(onSuccess1).then(showResult1);
      function onSuccess1(response1){
        return response1.json();
      };
      function showResult1(resultSkripsiKe){
        // bila ada npm yang tersedia di tahun akademik sekian dan skripsi ke sekian
        if (resultSkripsiKe.length != 0) {
          // buat option isinya skripsi keberapa
          const option = document.createElement('option');
          option.value = i;
          option.textContent = i;
          sidangKe.appendChild(option);
        }
      }
    }
    sidangKe.removeAttribute('disabled');
  };
  valid();
});

sidangKe.addEventListener('click', function(){
  const get_thnAkad = thnAkad.value;
  const get_sidangKe = sidangKe.value;
  const data = {
    thnAkad: get_thnAkad,
    sdgKe: get_sidangKe
  };
  const params = new URLSearchParams(data);
  const q = params.toString();
  const url = "/getNPM?" + q;
  
  // kirim tahun akademik dan skripsi ke-
  fetch(url).then(onSuccess).then(showResult);
  function onSuccess(response){
    return response.json();
  };
  function showResult(resultNPM){
    if (resultNPM.length != 0){
      npm.innerHTML='';
      for(let i of resultNPM){
        // buat option isinya npm mahasiswa
        const option = document.createElement('option');
        option.value = i[9];
        option.textContent = i[9];
        npm.appendChild(option);
      }
      npm.removeAttribute('disabled');
    } else {
      npm.setAttribute('disabled', true);
      npm.innerHTML='';
    }
  };
  valid();
});

npm.addEventListener('click', function(){
  valid();
});

//--------------------------------------------------------

const catatan = document.getElementById("catatan");
const submit = document.getElementById("submit");
const alertp = document.getElementById("catKosong");

catatan.addEventListener("input", function(){
  // tombol submit akan didisabled bila catatan belum terisi
  if (catatan.value == ""){
    alertp.style.display = "block";
    submit.setAttribute('disabled', true);
    submit.style.opacity = "0.5";
  } else {
    alertp.style.display = "none";
    submit.removeAttribute('disabled');
    submit.style.opacity = "1";
  }
});

submit.addEventListener("click", function(){
  const data = {
    catatan: catatan.value
  };
  const params = new URLSearchParams(data);
  const q = params.toString();
  const url1 = "/uploadCatatan?" + q;
  
  // kirim data berisi catatan
  fetch(url1).then(onSuccess1);
  function onSuccess1(response){
    return response.json();
  };

  alert("Berhasil upload catatan sidang!");
});